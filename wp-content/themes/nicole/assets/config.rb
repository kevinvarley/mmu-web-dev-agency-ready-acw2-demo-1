http_path = "/"

css_dir = "css"
sass_dir = "scss"
images_dir = "images"
javascripts_dir = "js"
environment = :development
relative_assets = true

output_style = :expanded
# output_style = :compressed

preferred_syntax = :scss
