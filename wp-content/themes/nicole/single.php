<?php get_header(); ?>

<main id="site-main" class="site-main" role="main">
  <div class="container">
    <div class="grid">
      <div class="grid__col grid__col--2-of-3">

        <?php
          if( have_posts() ) :
            while( have_posts() ) :
              the_post();
        ?>
              <div class="post-wrapper">
                <div class="post-header">
                  <h3 class="post-title"><a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></h3>
                  <span class="post-date"><?php the_date(); ?></span>
                </div>
                <div class="post-body">
                  <?php the_content(); ?>
                </div>
              </div>
        <?php
            endwhile;
          endif;
        ?>

      </div>

      <div class="grid__col grid__col--1-of-3">
        <?php get_sidebar(); ?>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>
