			</div> <!-- /container -->
		</main> <!-- /main -->

		<footer class="primary-footer" role="contentinfo">
			<div class="container">
					<small class="footer-copyright">
						<span class="footer-copyright-item">Copyright &copy; <?php echo bloginfo('name') . " " . date('Y'); ?> |</span>
					</small>
			</div>
		</footer>

	</div> <!-- /page -->
	<?php wp_footer(); ?>
</body>
</html>
