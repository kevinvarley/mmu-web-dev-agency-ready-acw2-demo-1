<?php

add_theme_support('menus');

add_action('init', 'register_site_menus');

function register_site_menus() {
  register_nav_menus (
    array (
      'primary_menu' => __('Primary Menu'),
      'secondary_menu' => __('Secondary Menu')
    )
  );
}

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

// tell WordPress that our theme supports WooCommerce
add_theme_support('woocommerce');
