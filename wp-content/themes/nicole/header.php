<?php
  /*
  * @package WordPress
  * @subpackage Nicole
  * @since Nicole 0.1
  */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <title><?php wp_title(''); echo " - "; bloginfo('name'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!--[if lt IE 9]>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/vendor/js/html5.js"></script>
  <![endif]-->
  <link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/css/style.css">
  <?php wp_head(); ?>
</head>

<body <?php body_class('nicole'); ?>>
  <div id="page" class="site">
    <header class="primary-header hero" role="masthead">
      <nav id="primary-navigation" class="site-navigation primary-navigation primary-navigation-default" role="navigation">
        <div class="container">
          <!-- <button class="menu-toggle"><?php _e( 'Primary Menu', 'twentyfourteen' ); ?></button> -->
          <?php wp_nav_menu( array( 'theme_location' => 'primary_menu', 'menu_class' => 'nav-menu', 'container' => 'div', 'container_class' => 'container' ) ); ?>
        </div>
      </nav>

      <div class="container">
        <h1 class="site-title-headline">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-title-link">
            <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/assets/images/logo.png" alt="<?php bloginfo('name'); ?>">
          </a>
        </h1>
      </div>

      <nav id="secondary-navigation" class="site-navigation secondary-navigation secondary-navigation-default" role="navigation">
        <div class="container">
          <!-- <button class="menu-toggle"><?php _e( 'Secondary Menu', 'twentyfourteen' ); ?></button> -->
          <?php wp_nav_menu( array( 'theme_location' => 'secondary_menu', 'menu_class' => 'nav-menu', 'container' => 'div', 'container_class' => 'container' ) ); ?>
        </div>
      </nav>
    </header>

    <main id="primary-content" class="primary-content" role="main">
      <div class="container">
